# RISC-V

This is a simple VHDL implementation of the base RISC-V standard

## Checklists

### Components
- [X] Register File
- [X] RAM
- [X] ALU

### Stages
- [X] Fetch
- [X] Decode
- [ ] Execute
- [ ] Memory
- [ ] Writeback


### Misc
- [ ] NVC support
- [ ] Configurable makefile
