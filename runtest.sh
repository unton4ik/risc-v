#/bin/bash

if [ "$1" = "--all" ]; then
   for testfile in tests/*_TB.vhd; do
      testunit="${testfile: 6: -4}"
      echo 
      echo "Testing ${testunit: : -3}"
      ghdl -a -g --no-vital-checks --workdir=./ ./tests/${testunit}.vhd;  
      ghdl --elab-run --workdir=./ $testunit --wave=./tests/${testunit}.ghw --backtrace-severity=warning;
   done
else
   for unit in $@; do
      unit="${unit^^}"
      echo
      echo "Testing $unit"
      ghdl -a -g --no-vital-checks --workdir=./ ./tests/${unit}_TB.vhd;  
      ghdl --elab-run --workdir=./ ${unit}_TB --wave=./tests/${unit}_TB.ghw --backtrace-severity=warning;
   done

   # open waveform if only one test was given
   if test "$#" -eq 1; then
      gtkwave --dump=./tests/${1^^}_TB.ghw;
   fi
fi
