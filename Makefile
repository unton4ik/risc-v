VHDLC=ghdl
SRCDIR=./src
STD=93

ifeq ($(VHDLC),ghdl)
	FLAGS=--workdir=./
else ifeq ($(VHDLC),nvc)
	FLAGS=--relaxed
endif

.PHONY: all clean test

all: rv_lib reg reg_file ram alu pc stage_fetch stage_decode stage_execute stage_memory stage_writeback


rv_lib: $(SRCDIR)/rv_lib.vhd 
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/rv_lib.vhd

reg: $(SRCDIR)/REG.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/REG.vhd

reg_file: reg $(SRCDIR)/REG_FILE.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/REG_FILE.vhd

ram: $(SRCDIR)/RAM.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/RAM.vhd

pc: $(SRCDIR)/PC.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/PC.vhd

alu: rv_lib $(SRCDIR)/ALU.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/ALU.vhd

stage_fetch: rv_lib $(SRCDIR)/STAGE_FETCH.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/STAGE_FETCH.vhd

stage_decode: rv_lib $(SRCDIR)/STAGE_DECODE.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/STAGE_DECODE.vhd

stage_execute: rv_lib $(SRCDIR)/STAGE_EXECUTE.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/STAGE_EXECUTE.vhd

stage_memory: rv_lib $(SRCDIR)/STAGE_MEMORY.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/STAGE_MEMORY.vhd
	
stage_writeback: rv_lib $(SRCDIR)/STAGE_WRITEBACK.vhd
	$(VHDLC) -a $(FLAGS) $(SRCDIR)/STAGE_WRITEBACK.vhd

ghdl:
	make VHDLC=ghdl SRCDIR=$(SRCDIR) STD=$(STD)

nvc:
	make VHDLC=nvc SRCDIR=$(SRCDIR) STD=$(STD)

test:
	bash ./runtest.sh --all

clean:
	rm -rf work-obj*.cf *.o *.ghw tests/*.ghw work/
