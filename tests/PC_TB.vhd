--! PC entity testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity PC_TB is
end entity;

architecture tb of PC_TB is

   constant aw : integer := 6;
   constant dw : integer := 8;
   constant increment : integer := integer(ceil(real(dw)/8.0));

   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal en  : std_logic := '1';

   signal load_sim     : std_logic := '0';
   signal addr_in_sim  : std_logic_vector(aw-1 downto 0) := (others => '0');
   signal addr_out_sim : std_logic_vector(aw-1 downto 0) := (others => '0');

   signal done : boolean := false;

begin
   
   clk <= not clk after 1 ns;
   rst <= '0';
   en  <= '1';

   load_sim <= not load_sim after 5 ns;
   done <= false, true after 20 ns;

   dut : entity work.PC
   generic map (aw, dw)
   port map (
      CLK => clk,
      RST => rst,
      EN  => en,
      LOAD => load_sim,
      ADDR_IN => addr_in_sim,
      ADDR_OUT => addr_out_sim
   );

   stimulus : process (CLK)
   begin
      if falling_edge(CLK) then

         if done then
            report "simulation finished" severity failure;
         end if;

         addr_in_sim <= std_logic_vector(unsigned(addr_in_sim) + 3*increment);

      end if;
   end process;

end architecture;
