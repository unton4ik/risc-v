--! REG_FILE entity testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REG_FILE_TB is
end entity;

architecture tb of REG_FILE_TB is

   constant aw : integer := 3;
   constant dw : integer := 8;

   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

   signal re_a_sim : std_logic := '1';
   signal re_b_sim : std_logic := '0';
   signal we_sim : std_logic := '1';
   
   signal sel_a_sim : std_logic_vector(aw-1 downto 0) := (others => '0');
   signal sel_b_sim : std_logic_vector(aw-1 downto 0) := (others => '0');
   signal sel_w_sim : std_logic_vector(aw-1 downto 0) := (others => '0');

   signal data_in_sim  : std_logic_vector(dw-1 downto 0) := (others => '0');
   signal data_out_a_sim : std_logic_vector(dw-1 downto 0) := (others => '0');
   signal data_out_b_sim : std_logic_vector(dw-1 downto 0) := (others => '0');

   signal done : boolean := false;

begin

   clk <= not clk after 1 ns;
   rst <= '0'; -- '1' after TODO
   done <= true after 64 ns;

   re_a_sim <= not re_a_sim after 8 ns;
   re_b_sim <= not re_b_sim after 8 ns;
   we_sim <= not we_sim after 16 ns;

   -- sel_a_sim <= std_logic_vector(unsigned(sel_a_sim) + 1) after 2 ns;

   dut : entity work.REG_FILE
   generic map (aw, dw)
   port map (
      clk,
      rst,
      re_a_sim,
      re_b_sim,
      we_sim,
      sel_a_sim,
      sel_b_sim,
      sel_w_sim,
      data_in_sim,
      data_out_a_sim,
      data_out_b_sim
   );

   stimulus : process
   begin
      wait until falling_edge(CLK);
      
      if done then
         report "simulation finished" severity failure;
      end if;
      
      sel_a_sim   <= std_logic_vector(unsigned(sel_a_sim) + 1);
      sel_b_sim   <= std_logic_vector(unsigned(sel_b_sim) + 1);
      sel_w_sim   <= std_logic_vector(unsigned(sel_w_sim) + 1);
      data_in_sim <= std_logic_vector(unsigned(data_in_sim) + 1);

   end process;

end architecture;
