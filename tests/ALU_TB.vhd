--! ALU entity testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.ALU;
use work.rv_lib.all;

entity ALU_TB is
end entity;

architecture tb of ALU_TB is

   signal clk : std_logic := '0';

   signal op_sel_sim : ARITH_OP_T := OP_ADD_SUB;
   signal op_mod_sim : std_logic_vector(6 downto 0) := (others => '0');

   signal a_sim : std_logic_vector(XLEN-1 downto 0) := (others => '0');
   signal b_sim : std_logic_vector(XLEN-1 downto 0) := (others => '0');

   signal res_sim : std_logic_vector(XLEN-1 downto 0) := (others => '0');
   signal z_sim : std_logic := '0';
   signal c_sim : std_logic := '0';
   signal e_sim : std_logic := '0';

   signal test_pass : std_logic := '0';

   signal done : boolean := false;

begin
   
   clk <= not clk after 1 ns;

   -- done <= false, true after 100 ns;

   dut : entity ALU
   port map (
      op_sel_sim,
      op_mod_sim,
      a_sim,
      b_sim,
      res_sim,
      z_sim,
      c_sim,
      e_sim
   );

   process
      variable op1 : integer := 123;
      variable op2 : integer := 321;
   begin

      if done /= true then

         a_sim <= std_logic_vector(to_unsigned(op1, a_sim'length));
         b_sim <= std_logic_vector(to_unsigned(op2, b_sim'length));

         -- test addition
         --report "Testing addition" severity note;
         op_sel_sim <= OP_ADD_SUB;
         op_mod_sim <= "0000000";

         --if to_integer(unsigned(res_sim)) = op1 + op2 then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;

         wait for 5 ns;
       
       
         -- test subtraction
         --report "Testing subtraction" severity note;
         op_sel_sim <= OP_ADD_SUB;
         op_mod_sim <= "0100000";
       
         --if signed(res_sim) = to_signed(op1, a_sim'length) - to_signed(op2, b_sim'length) then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;
       
         wait for 5 ns;
       

         -- test shift left
         op_sel_sim <= OP_SLL;
         op_mod_sim <= "0000000";
       
         --if unsigned(res_sim) = shift_left(unsigned(a_sim), to_integer(unsigned(b_sim))) then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;
       
         wait for 5 ns;
       

         -- test shift right
         op_sel_sim <= OP_SRL_SRA;
         op_mod_sim <= "0000000";
       
         --if unsigned(res_sim) = shift_right(unsigned(a_sim), to_integer(unsigned(b_sim))) then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;

         wait for 5 ns;


         op_sel_sim <= OP_SRL_SRA;
         op_mod_sim <= "0100000";

         --if signed(res_sim) = shift_right(signed(a_sim), to_integer(unsigned(b_sim))) then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;

         wait for 5 ns;


         -- test less than
         op_sel_sim <= OP_SLT;
         op_mod_sim <= "0000000";

         --if (res_sim = (0 => '1', others => '0') and a_sim < b_sim) or (res_sim = (others => '0') and a_sim >= b_sim) then
         --   test_pass <= '1';
         --   report "   Pass" severity note;
         --else
         --   test_pass <= '0';
         --   report "   Fail: " & integer'image(to_integer(unsigned(res_sim))) severity note;
         --end if;

         wait for 5 ns;


         -- test less than unsigned
         op_sel_sim <= OP_SLTU;
         op_mod_sim <= "0000000";
         wait for 5 ns;


         -- test XOR
         op_sel_sim <= OP_XOR;
         op_mod_sim <= "0000000";
         wait for 5 ns;


         -- test OP
         op_sel_sim <= OP_OR;
         op_mod_sim <= "0000000";
         wait for 5 ns;


         --test AND
         op_sel_sim <= OP_AND;
         op_mod_sim <= "0000000";
         wait for 5 ns;


         report "simulation finished" severity failure;

      end if;
   end process;

end architecture;
