--! RAM entity testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM_TB is
end entity;

architecture tb of RAM_TB is
   constant aw : natural := 3;
   constant dw : natural := 8;

   constant start_a : integer := 0;
   constant start_b : integer := 2**aw/2;

   signal clk : std_logic := '0';

   signal we_a_sim : std_logic := '1';    
   signal we_b_sim : std_logic := '0'; 

   signal addr_a_sim     : std_logic_vector(aw-1 downto 0) := std_logic_vector(to_unsigned(start_a, aw));
   signal addr_b_sim     : std_logic_vector(aw-1 downto 0) := std_logic_vector(to_unsigned(start_b, aw));

   signal data_in_a_sim  : std_logic_vector(dw-1 downto 0) := (others => '0');
   signal data_in_b_sim  : std_logic_vector(dw-1 downto 0) := (others => '0');

   signal data_out_a_sim : std_logic_vector(dw-1 downto 0) := (others => '0');
   signal data_out_b_sim : std_logic_vector(dw-1 downto 0) := (others => '0');

   signal done : boolean := false;

begin

   clk <= not clk after 1 ns;
   done <= true after 32 ns;

   we_a_sim <= not we_a_sim after 4 ns;
   we_b_sim <= not we_b_sim after 4 ns;

   dut : entity work.RAM
   generic map (aw, dw)
   port map(
      clk,
      we_a_sim,
      we_b_sim,
      addr_a_sim,
      addr_b_sim,
      data_in_a_sim,
      data_in_b_sim,
      data_out_a_sim,
      data_out_b_sim
   );

   process (CLK, done)
   begin
      if done then
         report "simulation finished" severity failure;
      end if;

      if falling_edge(CLK) then
         addr_a_sim <= std_logic_vector(unsigned(addr_a_sim) + 1);
         addr_b_sim <= std_logic_vector(unsigned(addr_b_sim) + 1);

         data_in_a_sim <= std_logic_vector(unsigned(data_in_a_sim) + 5);         
         data_in_b_sim <= std_logic_vector(unsigned(data_in_b_sim) + 5);
      end if;
   end process;

end architecture;
