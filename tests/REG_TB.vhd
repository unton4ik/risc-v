--! REG entity testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REG_TB is
end entity;

architecture tb of REG_TB is

   constant dw : integer := 4;

   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

   signal re_sim : std_logic := '1';
   signal we_sim : std_logic := '1';
    
   signal data_in_sim  : std_logic_vector(dw   downto 0) := (others => '0');
   signal data_out_sim : std_logic_vector(dw-1 downto 0) := (others => '0');

   signal done : boolean := false;

begin

   clk <= not clk after 1 ns;
   rst <= '0'; --, '1' after 5 ns;
   done <= true after 32 ns;

   re_sim <= not re_sim after 16 ns;
   we_sim <= not we_sim after 8 ns;

   dut : entity work.REG
   generic map (dw)
   port map (
      CLK => clk,
      RST => rst,
      RE => re_sim,
      WE => we_sim,
      DATA_IN => data_in_sim(dw-1 downto 0),
      DATA_OUT => data_out_sim 
   );

   stimulus : process (CLK)
   begin
      if falling_edge(CLK) then

         -- if data_in_sim = "11111" then
         --    -- data_in_sim <= "0000";
         --    report "simulation finished" severity failure;
         -- end if;

         if done then
            report "simulation finished" severity failure;
         end if;

         -- for i in 0 to dw-1 loop
         data_in_sim <= std_logic_vector(unsigned(data_in_sim) + 1);
         -- end loop;
      end if;
   end process;

end architecture;
