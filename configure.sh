#/bin/bash

VHDLC=${VHDLC:-ghdl}
SRCDIR=${SRCDIR:-./src}
STD=${STD:-93}

valid_stds_ghdl="87 93 02 08 1987 1993 2002 2008"
valid_stds_nvc="93 00 02 08 19 2000 2002 2008 2019"

contains() {
    if [[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]]; then
       return 1;
    else
       return 0;
    fi;
};

if [ "$VHDLC" = "ghdl" ]; then
   if ! contains $valid_stds_ghdl $STD; then
      echo "Bad std: $STD";
      echo "Valid std options are: $valid_stds_ghdl";
      exit 1;
   fi;
elif [ "$VHDLC" = "nvc" ]; then
   if ! contains $valid_stds_nvc $STD; then
      echo "Bad std: $STD";
      echo "Valid std options are: $valid_stds_nvc";
      exit 1;
   fi;
else
   echo "Bad compiler: $VHDLC";
   exit 1;
fi;

echo $valid_stds_nvc
