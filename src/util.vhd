library ieee;
use ieee.std_logic_1164.all;

package util is

   function plus(L : std_logic_vector; R : std_logic_vector) return std_logic_vector;
   function minus(L : std_logic_vector; R : std_logic_vector) return std_logic_vector;

end package;

package body util is

   function to_string(slv : std_logic_vector) return string is
      variable slv_s : string(slv'length-1 downto 0) := (others => '0');
   begin
      for i in 0 to slv'length-1 loop
         if slv(i) = '1' then
            slv_s(i) := '1';
         else
            slv_s(i) := '0';
         end if;
      end loop;

      return slv_s;
   end function;



   function plus(L : std_logic_vector; R : std_logic_vector) return std_logic_vector is
      variable carry : std_ulogic;
      variable BV, sum : std_logic_vector(L'length-1 downto 0);
   begin
      if (L(L'left) = 'X' or R(R'left) = 'X') then
         sum := (others => 'X');
         return sum;
      end if;

      carry := '0';
      BV := R;

      for i in 0 to L'left loop
         sum(i) := L(i) xor BV(i) xor carry;
         carry := (L(i) and BV(i)) or
            (L(i) and carry) or
            (carry and BV(i));
      end loop;

      return sum;
   end function;

   function minus(L : std_logic_vector; R : std_logic_vector) return std_logic_vector is
      variable carry : std_ulogic;
      variable BV, sum : std_logic_vector(L'length-1 downto 0);
   begin
      report "minus begin";
      if (L(L'left) = 'X' or R(R'left) = 'X') then
         sum := (others => 'X');
         return sum;
      end if;

      carry := '1';
      BV := not R;

      report "start for loop";
      for i in 0 to L'length-1 loop
         report "    " & to_string(sum);
         sum(i) := L(i) xor BV(i) xor carry;
         carry := (L(i) and BV(i)) or
            (L(i) and carry) or
            (carry and BV(i));
      end loop;

      report "operands:";
      report to_string(L);
      report to_string(R);
      report "result:";
      report to_string(sum);
      

      return sum;
   end function;

end package body;
