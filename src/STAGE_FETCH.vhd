--! Fetch Stage Control Unit

-- The fetch stage will:
-- 1. get the instruction from RAM pointed to by PC
-- 2. increment the PC
-- 

library ieee;
use ieee.std_logic_1164.all;

use work.rv_lib.all;

entity STAGE_FETCH is
    -- generic (
    --     -- op width
    --     -- dw : integer := 32
    -- );
   port (
      CLK : in std_logic;
      RST : in std_logic;

      --! Instruction input from RAM
      INSTR_IN   : in std_logic_vector(XLEN-1 downto 0);
      --! Current PC value
      PC_CUR_IN  : in std_logic_vector(XLEN-1 downto 0);
      --! Incremented PC value (*not needed?*)
      PC_NEXT_IN : in std_logic_vector(XLEN-1 downto 0);

      -- not needed?
      -- PC_EN : out std_logic

      --! Instruction for next stage
      INSTR_OUT  : out std_logic_vector(XLEN-1 downto 0);
      --! Current PC value for next stage
      PC_CUR_OUT : out std_logic_vector(XLEN-1 downto 0)
   );
end entity;

architecture rtl of STAGE_FETCH is

   signal instr_int  : std_logic_vector(XLEN-1 downto 0);
   signal pc_cur_int : std_logic_vector(XLEN-1 downto 0);

begin

   process (CLK, RST) is
   begin
      if RST = '1' then
         instr_int <= (others => '0');
         pc_cur_int <= (others => '0');
      elsif rising_edge(CLK) then
         instr_int <= INSTR_IN;
         pc_cur_int <= PC_CUR_IN;
      end if;
   end process;

   INSTR_OUT <= instr_int;
   PC_CUR_OUT <= pc_cur_int;

end architecture;
