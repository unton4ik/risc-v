--! Two-port RAM Block

-- Based on the Quartus Prime True Dual-Port RAM With Single Clock template

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM is

   generic (
      --! Address width
      aw : natural := 6;
      --! Data width
      dw : natural := 8
   );

   port (
      CLK  : in std_logic;

      --! Port A write enable
      WE_A : in std_logic;
      --! Port B write enable
      WE_B : in std_logic;

      --! Port A address
      ADDR_A : in std_logic_vector(aw-1 downto 0);
      --! Port B address
      ADDR_B : in std_logic_vector(aw-1 downto 0);

      --! Port A input data
      DATA_IN_A : in std_logic_vector(dw-1 downto 0);
      --! Port B input data
      DATA_IN_B : in std_logic_vector(dw-1 downto 0);

      --! Port A output data
      DATA_OUT_A  : out std_logic_vector(dw-1 downto 0);
      --! Port B output data
      DATA_OUT_B  : out std_logic_vector(dw-1 downto 0)
   );

end RAM;


architecture rtl of RAM is

   --! Word type
   subtype word_t is std_logic_vector((dw-1) downto 0);
   --! Memory array type
   type memory_t is array(2**aw-1 downto 0) of word_t;

   shared variable memory : memory_t := (others => (others => '0'));
   signal a_idx : integer;
   signal b_idx : integer;

begin


   -- Port A
   process(CLK)
   begin
      if rising_edge(CLK) then 
         if(WE_A = '1') then
            memory(a_idx) := DATA_IN_A;
         end if;
         DATA_OUT_A <= memory(a_idx);
      end if;
   end process;

   -- Port B 
   process(CLK)
   begin
      if rising_edge(CLK) then 
         if(WE_B = '1') then
            memory(b_idx) := DATA_IN_B;
         end if;
         DATA_OUT_B <= memory(b_idx);
      end if;
   end process;

   a_idx <= to_integer(unsigned(ADDR_A));
   b_idx <= to_integer(unsigned(ADDR_B));

end rtl;
