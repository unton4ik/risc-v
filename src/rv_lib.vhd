--! RISC-V Types Library

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package rv_lib is

   --! Integer register width
   constant XLEN : integer := 32;
   --! Register address width
   constant RLEN : integer := 5;

   --! Enumeration of supported instruction formats
   type IFORMAT is (IFORMAT_R, IFORMAT_I, IFORMAT_S, IFORMAT_B, IFORMAT_U, IFORMAT_J);

   --! R-type instruction
   type OP_R is record
      opcode : std_logic_vector(6 downto 0);
      rd     : std_logic_vector(4 downto 0);
      funct3 : std_logic_vector(2 downto 0);
      rs1    : std_logic_vector(4 downto 0);
      rs2    : std_logic_vector(4 downto 0);
      funct7 : std_logic_vector(6 downto 0);
   end record;

   --! I-type instruction
   type OP_I is record
      opcode  : std_logic_vector(6 downto 0);
      rd      : std_logic_vector(4 downto 0);
      funct3  : std_logic_vector(2 downto 0);
      rs1     : std_logic_vector(4 downto 0);
      imm11_0 : std_logic_vector(11 downto 0);
   end record;

   --! S-type instruction
   type OP_S is record
      opcode  : std_logic_vector(6 downto 0);
      imm4_0  : std_logic_vector(4 downto 0);
      funct3  : std_logic_vector(2 downto 0);
      rs1     : std_logic_vector(4 downto 0);
      rs2     : std_logic_vector(4 downto 0);
      imm11_5 : std_logic_vector(6 downto 0);
   end record;

   --! B-type instruction
   type OP_B is record
      opcode  : std_logic_vector(6 downto 0);
      imm11   : std_logic;
      imm4_1  : std_logic_vector(3 downto 0);
      funct3  : std_logic_vector(2 downto 0);
      rs1     : std_logic_vector(4 downto 0);
      rs2     : std_logic_vector(4 downto 0);
      imm10_5 : std_logic_vector(5 downto 0);
      imm12   : std_logic;
   end record;

   --! U-type instruction
   type OP_U is record
      opcode   : std_logic_vector(6 downto 0);
      rd       : std_logic_vector(4 downto 0);
      imm31_12 : std_logic_vector(19 downto 0);
   end record;

   --! J-type instruction
   type OP_J is record
      opcode   : std_logic_vector(6 downto 0);
      rd       : std_logic_vector(4 downto 0);
      imm19_12 : std_logic_vector(7 downto 0);
      imm11    : std_logic;
      imm10_1  : std_logic_vector(9 downto 0);
      imm20    : std_logic;
   end record;

   attribute ENUM_ENCODING : string;

   --! Enumeration of supported opcodes
   type OPCODE_T is (
      OP_IMM,
      OP_LUI,
      OP_AUIPC,
      OP_OP,
      OP_JAL,
      OP_JALR,
      OP_BRANCH,
      OP_LOAD,
      OP_STORE,
      OP_SYSTEM,
      OP_MISC_MEM,
      OP_INVALID
   );
     
   attribute enum_encoding of OPCODE_T : type is
      "0010011 " &   -- OP_IMM
      "0110111 " &   -- OP_LUI
      "0010111 " &   -- OP_AUIPC
      "0110011 " &   -- OP_OP
      "1101111 " &   -- OP_JAL
      "1100111 " &   -- OP_JALR
      "1100011 " &   -- OP_BRANCH
      "0000011 " &   -- OP_LOAD
      "0100011 " &   -- OP_STORE
      "1110011 " &   -- OP_SYSTEM
      "0001111 " &   -- OP_MISC_MEM
      "1111111";     -- OP_INVALID

   --! Enumeration of supported branch operations
   type BRANCH_OP_T is (
      OP_BEQ,
      OP_BNE,
      OP_BLT,
      OP_BGE,
      OP_BLTU,
      OP_BGEU
   );

   attribute enum_encoding of BRANCH_OP_T : type is
      "000 " & -- BEQ
      "001 " & -- BNE
      "100 " & -- BLT
      "101 " & -- BGE
      "110 " & -- BLTU
      "111";   -- BGEU

   --! Enumeration of supported load operations
   type LOAD_OP_T is (
      OP_LB,
      OP_LH,
      OP_LW,
      OP_LBU,
      OP_LHU
   );

   attribute enum_encoding of LOAD_OP_T : type is
      "000 " & -- LB
      "001 " & -- LH
      "010 " & -- LW
      "100 " & -- LBU
      "101";   -- LHU

   --! Enumeration of supported store operations
   type STORE_OP_T is (
      OP_SB,
      OP_SH,
      OP_SW
   );

   attribute enum_encoding of STORE_OP_T : type is
      "000 " & -- SB
      "001 " & -- SH
      "010";   -- SW

--   --! Enumeration of supported immediate arithmetic operations
--   type IMM_OP_T is (
--      OP_ADDI,
--      OP_SLTI,
--      OP_SLTIU,
--      OP_XORI,
--      OP_ORI,
--      OP_ANDI,
--      OP_SLLI,
--      OP_SRLI_SRAI
--   );

--   attribute enum_encoding of IMM_OP_T : type is
--      "000 " & -- ADDI
--      "001 " & -- SLLI
--      "010 " & -- SLTI
--      "011 " & -- SLTIU
--      "100 " & -- XORI
--      "110 " & -- ORI
--      "111 " & -- ANDI
--      "101";   -- SRLI_SRAI

   --! Enumeration of supported arithmetic operations
   type ARITH_OP_T is (
      OP_ADD_SUB,
      OP_SLL,
      OP_SLT,
      OP_SLTU,
      OP_XOR,
      OP_SRL_SRA,
      OP_OR,
      OP_AND
   );

   attribute enum_encoding of ARITH_OP_T : type is
      "000 " & -- ADD_SUB
      "001 " & -- SLL
      "010 " & -- SLT
      "011 " & -- SLTU
      "100 " & -- XOR
      "101 " & -- SRL_SRA
      "110 " & -- OR
      "111";   -- AND


--   --! Enumeration of supported system call operations
--   type SYS_OP_T is (
--      OP_ECALL,
--      OP_EBREAK
--   );
--
--   --! Enumeration of supported misc memory operations
--   type MISC_MEM_OP_T is (
--      OP_FENCE
--   );


   --! Determines the instruction format based on opcode
   function format_decode(slv : std_logic_vector(XLEN-1 downto 0)) return IFORMAT;
    
   --! Decodes an R-type instruction
   function op_decode_r(slv : std_logic_vector(XLEN-1 downto 0)) return OP_R;
    
   --! Decodes an I-type instruction
   function op_decode_i(slv : std_logic_vector(XLEN-1 downto 0)) return OP_I;
    
   --! Decodes an S-type instruction
   function op_decode_s(slv : std_logic_vector(XLEN-1 downto 0)) return OP_S;
    
   --! Decodes a B-type instruction
   function op_decode_b(slv : std_logic_vector(XLEN-1 downto 0)) return OP_B;
     
   --! Decodes a U-type instruction
   function op_decode_u(slv : std_logic_vector(XLEN-1 downto 0)) return OP_U;
     
   --! Decodes a J-type instruction
   function op_decode_j(slv : std_logic_vector(XLEN-1 downto 0)) return OP_J;

end package rv_lib;

package body rv_lib is
    --! Determines the instruction format based on opcode
   function format_decode(slv : std_logic_vector(XLEN-1 downto 0))
   return IFORMAT is
      variable opcode : OPCODE_T := OP_INVALID;
   begin
      opcode := OPCODE_T'val(to_integer(unsigned(slv)));

      if opcode = OP_OP then
         return IFORMAT_R;
      elsif opcode = OP_JALR or opcode = OP_LOAD or opcode = OP_IMM
      or opcode = OP_SYSTEM or opcode = OP_MISC_MEM then
         return IFORMAT_I;
      elsif opcode = OP_STORE then
         return IFORMAT_S;
      elsif opcode = OP_BRANCH then
         return IFORMAT_B;
      elsif opcode = OP_LUI or opcode = OP_AUIPC then
         return IFORMAT_U;
      elsif opcode = OP_JAL then
         return IFORMAT_J;
      end if;
   end format_decode;


   --! Decodes an R-type instruction
   function op_decode_r(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_R is
      variable op : OP_R;
   begin
      op.opcode := slv(6 downto 0);
      op.rd     := slv(11 downto 7);
      op.funct3 := slv(14 downto 12);
      op.rs1    := slv(19 downto 15);
      op.rs2    := slv(24 downto 20);
      op.funct7 := slv(XLEN-1 downto 25);

      return op;
   end op_decode_r;

   --! Decodes an I-type instruction
   function op_decode_i(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_I is
      variable op : OP_I;
   begin
      op.opcode  := slv(6 downto 0);
      op.rd      := slv(11 downto 7);
      op.funct3  := slv(14 downto 12);
      op.rs1     := slv(19 downto 15);
      op.imm11_0 := slv(XLEN-1 downto 20);

      return op;
   end op_decode_i;

   --! Decodes an S-type instruction
   function op_decode_s(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_S is
      variable op : OP_S;
   begin
      op.opcode  := slv(6 downto 0);
      op.imm4_0  := slv(11 downto 7);
      op.funct3  := slv(14 downto 12);
      op.rs1     := slv(19 downto 15);
      op.rs2     := slv(24 downto 20);
      op.imm11_5 := slv(XLEN-1 downto 25);

      return op;
   end op_decode_s;

   --! Decodes a B-type instruction
   function op_decode_b(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_B is
      variable op : OP_B;
   begin
      op.opcode  := slv(6 downto 0);
      op.imm11   := slv(7);
      op.imm4_1  := slv(11 downto 8);
      op.funct3  := slv(14 downto 12);
      op.rs1     := slv(19 downto 15);
      op.rs2     := slv(24 downto 20);
      op.imm10_5 := slv(30 downto 25);
      op.imm12   := slv(31);

      return op;
   end op_decode_b;

   --! Decodes a U-type instruction
   function op_decode_u(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_U is
      variable op : OP_U;
   begin
      op.opcode   := slv(6 downto 0);
      op.rd       := slv(11 downto 7);
      op.imm31_12 := slv(XLEN-1 downto 12);

      return op;
   end op_decode_u;

   --! Decodes a J-type instruction
   function op_decode_j(slv : std_logic_vector(XLEN-1 downto 0))
   return OP_J is
      variable op : OP_J;
   begin
      op.opcode   := slv(6 downto 0);
      op.rd       := slv(11 downto 7);
      op.imm19_12 := slv(19 downto 12);
      op.imm11    := slv(20);
      op.imm10_1  := slv(30 downto 21);
      op.imm20    := slv(31);

      return op;
   end op_decode_j;

end package body;
