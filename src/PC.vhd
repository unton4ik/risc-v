--! Program Counter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity PC is
   generic (
      --! Address width
      aw : integer := 6;
      --! Data width
      dw : integer := 8
   );
   port (
      CLK  : in std_logic;
      RST  : in std_logic;
      EN   : in std_logic;

      --! Load new address flag
      LOAD    : in std_logic;
      --! New branch/jump address
      ADDR_IN : in std_logic_vector(aw-1 downto 0);

      --! Execution point
      ADDR_OUT : out std_logic_vector(aw-1 downto 0)
   );
end entity;

architecture rtl of PC is
   signal addr_int : std_logic_vector(aw-1 downto 0) := (others => '0');
   constant increment : integer := integer(ceil(real(dw)/8.0));
begin

   process (CLK, RST, EN, LOAD)
   begin
      if RST = '1' then
         addr_int <= (others => '0');
      elsif rising_edge(CLK) then
         if LOAD = '1' then
            addr_int <= ADDR_IN;
         elsif EN = '1' then
            -- increment conforming to byte boundary
            addr_int <= std_logic_vector(unsigned(addr_int) + increment);
         end if;
      end if;
   end process;

   ADDR_OUT <= addr_int;

end architecture;
