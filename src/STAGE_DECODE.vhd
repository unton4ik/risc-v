--! Decode Stage Control Unit

-- The decode stage will:
-- 1. decode the instruction opcode
-- 2. select the required registers and/or decode the immediate values
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.rv_lib.all;

entity STAGE_DECODE is
   port (
      
      CLK : in std_logic;
      RST : in std_logic;

      --! Machine code instruction
      INSTR : in std_logic_vector(XLEN-1 downto 0);

      --! Source register 1
      RS1 : out std_logic_vector(4 downto 0);
      --! Source register 2
      RS2 : out std_logic_vector(4 downto 0);
      --! Destination register
      RD  : out std_logic_vector(4 downto 0);
      --! Immediate value
      IMM : out std_logic_vector(XLEN-1 downto 0);

      --! Instruction opcode
      OP : out OPCODE_T;
      --! Function code
      FUNCT : out std_logic_vector(9 downto 0)

   );
end entity;

architecture rtl of STAGE_DECODE is

   signal format : IFORMAT;

   signal op_int    : OPCODE_T;
   signal funct_int : std_logic_vector(9 downto 0) := (others => '0');
   signal rs1_int   : std_logic_vector(4 downto 0) := (others => '0');
   signal rs2_int   : std_logic_vector(4 downto 0) := (others => '0');
   signal rd_int    : std_logic_vector(4 downto 0) := (others => '0');
   signal imm_int   : std_logic_vector(XLEN-1 downto 0) := (others => '0');

begin

   format <= format_decode(INSTR);

   process (CLK, RST, format) is
      variable op_r_var : OP_R;
      variable op_i_var : OP_I;
      variable op_s_var : OP_S;
      variable op_b_var : OP_B;
      variable op_u_var : OP_U;
      variable op_j_var : OP_J;
   begin

      if RST = '1' then
         rs1_int   <= (others => '0');
         rs2_int   <= (others => '0');
         rd_int    <= (others => '0');
         imm_int   <= (others => '0');
         op_int    <= OP_INVALID;
         funct_int <= (others => '0');

      elsif rising_edge(CLK) then
         op_r_var := (others => (others => 'Z'));
         op_i_var := (others => (others => 'Z'));
         op_s_var := (others => (others => 'Z'));
         op_b_var := (imm11  => 'Z',
                      imm12  => 'Z',
                      others => (others => 'Z'));
         op_u_var := (others => (others => 'Z'));

         case format is
            when IFORMAT_R =>
               op_r_var  := op_decode_r(INSTR);

               op_int    <= OPCODE_T'val(to_integer(unsigned(op_r_var.opcode)));
               rd_int    <= op_r_var.rd;
               rs1_int   <= op_r_var.rs1;
               rs2_int   <= op_r_var.rs2;
               funct_int <= op_r_var.funct7 & op_r_var.funct3;

            when IFORMAT_I =>
               op_i_var  := op_decode_i(INSTR);

               op_int    <= OPCODE_T'val(to_integer(unsigned(op_i_var.opcode)));
               rd_int    <= op_i_var.rd;
               funct_int <= (9 downto 3 => '0') & op_i_var.funct3;
               rs1_int   <= op_i_var.rs1;
               imm_int   <= (XLEN-1 downto 12 => op_i_var.imm11_0(11))
                            & op_i_var.imm11_0;


            when IFORMAT_S =>
               op_s_var  := op_decode_s(INSTR);

               op_int    <= OPCODE_T'val(to_integer(unsigned(op_s_var.opcode)));
               funct_int <= (9 downto 3 => '0') & op_s_var.funct3;
               rs1_int   <= op_s_var.rs1;
               rs2_int   <= op_s_var.rs2;
               imm_int   <= (XLEN-1 downto 12 => op_s_var.imm11_5(6))
                            & op_s_var.imm11_5
                            & op_s_var.imm4_0;

            -- TODO: double check IMM indexing for B, U, J
            when IFORMAT_B =>
               op_b_var  := op_decode_b(INSTR);

               op_int    <= OPCODE_T'val(to_integer(unsigned(op_b_var.opcode)));
               funct_int <= (9 downto 3 => '0')
                            & op_b_var.funct3;
               rs1_int   <= op_b_var.rs1;
               rs2_int   <= op_b_var.rs2;
               imm_int   <= (XLEN-1 downto 12 => op_b_var.imm12)
                            & op_b_var.imm12
                            & op_b_var.imm11
                            & op_b_var.imm10_5
                            & op_b_var.imm4_1
                            & '0';

            when IFORMAT_U =>
               op_u_var := op_decode_u(INSTR);

               op_int   <= OPCODE_T'val(to_integer(unsigned(op_u_var.opcode)));
               rd_int   <= op_u_var.rd;
               imm_int  <= op_u_var.imm31_12
                           & (11 downto 0 => '0');

            when IFORMAT_J =>
               op_j_var := op_decode_j(INSTR);
   
               op_int   <= OPCODE_T'val(to_integer(unsigned(op_j_var.opcode)));
               rd_int   <= op_j_var.rd;
               imm_int  <= (XLEN-1 downto 21 => op_j_var.imm20)
                           & op_j_var.imm20
                           & op_j_var.imm19_12
                           & op_j_var.imm11
                           & op_j_var.imm10_1
                           & '0';

            when others =>
               null;
               
         end case;
      end if;
   end process;

   RS1   <= rs1_int;
   RS2   <= rs2_int;
   RD    <= rd_int;
   IMM   <= imm_int;
   FUNCT <= funct_int;
   OP    <= op_int;

end architecture;
