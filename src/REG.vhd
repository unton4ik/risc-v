--! Integer Register

library ieee;
use ieee.std_logic_1164.all;

entity REG is
   generic (
      --! Data width
      dw : integer := 16
   );
   port (
      CLK : in std_logic;
      RST : in std_logic;

      --! Read enable
      RE  : in std_logic;
      --! Write enable
      WE  : in std_logic;

      --! Input data
      DATA_IN  : in  std_logic_vector(dw-1 downto 0);
      --! Output data
      DATA_OUT : out std_logic_vector(dw-1 downto 0)
   );
end entity;

architecture rtl of REG is
   signal data_int : std_logic_vector(dw-1 downto 0) := (others => '0');
begin

   process (CLK, RST, WE)
   begin
      if RST = '1' then
         data_int <= (others => '0');
      elsif rising_edge(CLK) and WE = '1' then
         data_int <= DATA_IN;
      end if;
   end process;

   DATA_OUT <= 
   data_int when RE = '1'
   else (others => 'Z');

   -- with RE select DATA_OUT <=
   --     data_int        when '1',
   --     (others => 'Z') when '0';


end rtl;
