--! Integer Register Bank

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity REG_FILE is
   generic (
      --! Address width
      aw : integer := 5;
      --! Data width
      dw : integer := 16
   );
   port (
      CLK : in std_logic;
      RST : in std_logic;

      --! Read enable
      RE_A  : in std_logic;
      RE_B  : in std_logic;
      --! Write enable
      WE  : in std_logic;
      --! Register selector
      SEL_A : in std_logic_vector(aw-1 downto 0);
      SEL_B : in std_logic_vector(aw-1 downto 0);
      SEL_W : in std_logic_vector(aw-1 downto 0);

      --! Input data
      DATA_IN  : in std_logic_vector(dw-1 downto 0);
      --! Output data
      DATA_OUT_A : out std_logic_vector(dw-1 downto 0);
      DATA_OUT_B : out std_logic_vector(dw-1 downto 0)
   );
end REG_FILE;

architecture rtl of REG_FILE is

   signal sel_a_i : integer := 0;
   signal sel_b_i : integer := 0;
   signal sel_w_i : integer := 0;

   type reg_file_t is array (2**aw-1 downto 0) of std_logic_vector(dw-1 downto 0);
   signal registers : reg_file_t := (others => (others => '0'));

begin

   process (CLK)
   begin
      if rising_edge(CLK) then
         if RST = '1' then
            registers <= (others => (others => '0'));
         else
            if WE = '1' and sel_w_i > 0 then
               registers(sel_w_i) <= DATA_IN;
            end if;
            
         end if;
      end if;
   end process;

   -- 2.1: Register x0 is hardwired to 0
   registers(0) <= (others  => '0');

   sel_a_i <= to_integer(unsigned(SEL_A));
   sel_b_i <= to_integer(unsigned(SEL_B));
   sel_w_i <= to_integer(unsigned(SEL_W));

   DATA_OUT_A <= registers(sel_a_i) when RE_A = '1'
               else (others => 'Z');
   DATA_OUT_B <= registers(sel_b_i) when RE_B = '1'
               else (others => 'Z');

end rtl;
