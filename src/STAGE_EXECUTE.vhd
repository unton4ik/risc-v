--! ***WIP***
--! Execute Stage Control Unit

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.rv_lib.all;
use work.REG_FILE;
use work.ALU;

entity STAGE_EXECUTE is
   port (
      CLK : in std_logic;
      RST : in std_logic;

      RS1 : in std_logic_vector(RLEN-1 downto 0);   
      RS2 : in std_logic_vector(RLEN-1 downto 0);
      RD  : in std_logic_vector(RLEN-1 downto 0);
      IMM : in std_logic_vector(XLEN-1 downto 0);

      OPCODE : in OPCODE_T;
      FUNCT  : in std_logic_vector(9 downto 0);

      PC_CUR  : in std_logic_vector(XLEN-1 downto 0);
      PC_NEXT : in std_logic_vector(XLEN-1 downto 0);

      PC_LOAD : out std_logic;
      PC_ADDR : out std_logic_vector(XLEN-1 downto 0)

   );
end entity;


architecture rtl of STAGE_EXECUTE is

   signal reg_re_a : std_logic := '0';
   signal reg_re_b : std_logic := '0';
   signal reg_we   : std_logic := '0';
   signal reg_sel_a : std_logic_vector(4 downto 0) := (others => '0');
   signal reg_sel_b : std_logic_vector(4 downto 0) := (others => '0');
   signal reg_sel_w : std_logic_vector(4 downto 0) := (others => '0');
   signal reg_in    : std_logic_vector(XLEN-1 downto 0) := (others => '0');
   signal reg_out_a : std_logic_vector(XLEN-1 downto 0) := (others => '0');
   signal reg_out_b : std_logic_vector(XLEN-1 downto 0) := (others => '0');

   signal alu_sel : ARITH_OP_T := OP_ADD_SUB;
   signal alu_mod : std_logic_vector(6 downto 0) := (others => '0');
   signal alu_inp_a : std_logic_vector(XLEN-1 downto 0) := (others => '0'); 
   signal alu_inp_b : std_logic_vector(XLEN-1 downto 0) := (others => '0'); 
   signal alu_res   : std_logic_vector(XLEN-1 downto 0) := (others => '0'); 
   signal alu_z : std_logic := '0'; 
   signal alu_c : std_logic := '0';
   signal alu_e : std_logic := '0';

begin

   REG_FILE_INST : entity REG_FILE
   generic map (RLEN, XLEN)
   port map (
      CLK, RST,
      reg_re_a, reg_re_b, reg_we,
      reg_sel_a, reg_sel_b, reg_sel_w,
      reg_in, reg_out_a, reg_out_b
   );

   ALU_INST : entity ALU
   port map (alu_sel, alu_mod, alu_inp_a, alu_inp_b, alu_res, alu_z, alu_c, alu_e);

   process(CLK, RST, OPCODE) is
      variable branch_op   : BRANCH_OP_T := OP_BEQ;
      variable branch_cond : boolean := false;
   begin
      if RST = '1' then

         reg_re_a <= '0';
         reg_re_b <= '0';
         reg_we   <= '0';
         reg_sel_a <= (others => '0'); 
         reg_sel_b <= (others => '0'); 
         reg_sel_w <= (others => '0'); 
         reg_in    <= (others => '0');
         reg_out_a <= (others => '0');
         reg_out_b <= (others => '0');

         alu_sel   <= OP_ADD_SUB;
         alu_mod   <= (others => '0');
         alu_inp_a <= (others => '0');
         alu_inp_b <= (others => '0');
         alu_res   <= (others => '0');
         alu_z <= '0'; 
         alu_c <= '0';
         alu_e <= '0';

         PC_LOAD <= '0';
         PC_ADDR <= (others => '0');
         
      elsif rising_edge(CLK) then
         case OPCODE is
            when OP_IMM =>
               alu_sel <= ARITH_OP_T'val(to_integer(unsigned(FUNCT(2 downto 0))));
               alu_mod <= FUNCT(9 downto 3);

               reg_sel_a <= RS1;
               alu_inp_a <= reg_out_a;
               reg_re_a  <= '1';

               alu_inp_b <= IMM;

               reg_sel_w <= RD;
               reg_in <= alu_res;
               reg_we <= '1';
               

            when OP_LUI =>
               reg_sel_w <= RD;
               reg_in <= IMM;
               reg_we <= '1';
               

            when OP_AUIPC =>
               alu_sel <= OP_ADD_SUB;

               alu_inp_a <= PC_CUR;
               alu_inp_b <= IMM;

               reg_sel_w <= RD;
               reg_in <= alu_res;
               reg_we <= '1';


            when OP_OP =>
               alu_sel <= ARITH_OP_T'val(to_integer(unsigned(FUNCT(2 downto 0))));
               alu_mod <= FUNCT(9 downto 3);

               reg_sel_a <= RS1;
               reg_sel_b <= RS2;
               reg_re_a <= '1';
               

            -- TODO instruction_address_misaligned exception
            when OP_JAL =>
               alu_sel <= OP_ADD_SUB;

               alu_inp_a <= PC_CUR;
               alu_inp_b <= IMM;

               reg_sel_w <= RD;
               reg_in <= PC_NEXT;
               reg_we <= '1';

               PC_ADDR <= alu_res;
               PC_LOAD <= '1';

               
            -- TODO instruction_address_misaligned exception
            when OP_JALR =>
               alu_sel <= OP_ADD_SUB;

               reg_sel_a <= RS1;
               alu_inp_a <= reg_out_a;
               reg_re_a <= '1';

               alu_inp_b <= IMM;

               reg_sel_w <= RD;
               reg_in <= PC_NEXT;
               reg_we <= '1';

               PC_ADDR <= alu_res;
               PC_LOAD <= '1';
               

            when OP_BRANCH =>
               reg_sel_a <= RS1;
               reg_sel_b <= RS2;
               reg_re_a <= '1';
               reg_re_b <= '1';

               branch_op := BRANCH_OP_T'val(to_integer(unsigned(FUNCT(2 downto 0))));

               -- case statement would ensure that all cases are covered, but this looks nicer
               if (branch_op = OP_BEQ and reg_out_a =  reg_out_b) or
                  (branch_op = OP_BNE and reg_out_a /= reg_out_b) or
                  (branch_op = OP_BLT and signed(reg_out_a) <  signed(reg_out_b)) or
                  (branch_op = OP_BGE and signed(reg_out_a) >= signed(reg_out_b)) or
                  (branch_op = OP_BLTU and unsigned(reg_out_a) <  unsigned(reg_out_b)) or
                  (branch_op = OP_BGEU and unsigned(reg_out_a) >= unsigned(reg_out_b)) then
                     branch_cond := true;
               end if;

               if branch_cond then
                  alu_sel <= OP_ADD_SUB;
                  alu_inp_a <= PC_CUR;
                  alu_inp_b <= IMM;

                  PC_ADDR <= alu_res;
                  PC_LOAD <= '1';
               end if;

               

            when OP_LOAD =>
               -- TODO
               null;
               

            when OP_STORE =>
               -- TODO
               null;
               

            when OP_SYSTEM =>
               -- TODO
               null;
               

            when OP_MISC_MEM =>
               -- TODO
               null;           
               

            when OP_INVALID =>
               -- TODO 
               null;
               

            when others =>
               null;
         end case;
      end if;
   end process;

end architecture;
