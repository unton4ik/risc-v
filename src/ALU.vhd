--! Arithmetic Logic Unit

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- use work.util.all;

use work.rv_lib.all;

entity ALU is
   port (
      --! Operation selector
      OP_SEL : in ARITH_OP_T;
      --! Operation modifier
      OP_MOD : in std_logic_vector(6 downto 0);

      --! Operand A
      A   : in std_logic_vector(XLEN-1 downto 0);
      --! Operand B
      B   : in std_logic_vector(XLEN-1 downto 0);

      --! Result
      RES : out std_logic_vector(XLEN-1 downto 0);

      --! Zero flag
      Z : out std_logic;
      --! Carry flag
      C : out std_logic;
      --! Error flag
      E : out std_logic
   );
end entity;

architecture rtl of ALU is
   signal res_int : unsigned(XLEN downto 0) := (others => '0');
   signal a_int   : unsigned(XLEN downto 0) := (others => '0');
   signal b_int   : unsigned(XLEN downto 0) := (others => '0');

   signal ofchk : std_logic_vector(2 downto 0) := (others => '0');

   signal e_int : boolean := false;
begin

   process (OP_SEL, OP_MOD, A, B, a_int, b_int, res_int)
   begin
      a_int <= unsigned('0' & A);
      b_int <= unsigned('0' & B);
      e_int <= false;

      case OP_SEL is
         when OP_ADD_SUB =>
            if OP_MOD = "0100000" then
               res_int <= a_int - b_int;
            else
               res_int <= a_int + b_int;
            end if;
         
         when OP_SLL =>
            res_int <= shift_left(a_int, to_integer(b_int));

         when OP_SRL_SRA =>
            if OP_MOD = "0100000" then
               res_int <= shift_right(a_int, to_integer(b_int));
            else
               res_int <= unsigned(shift_right(signed(a_int), to_integer(b_int)));
            end if;

         when OP_SLT =>
            if a_int < b_int then
               res_int <= (0 => '1', others => '0');
            else
               res_int <= (others => '0');
            end if;

         when OP_SLTU =>
            if unsigned(a_int) < unsigned(b_int) then
               res_int <= (0 => '1', others => '0');
            else
               res_int <= (others => '0');
            end if;
      
         when OP_XOR =>
            res_int <= a_int xor b_int;

         when OP_OR =>
            res_int <= a_int or b_int;

         when OP_AND =>
            res_int <= a_int and b_int;
         

         when others =>
            e_int <= true;

      end case;
   end process;

   ofchk <= a_int(a_int'high) & b_int(b_int'high) & res_int(res_int'high);

   RES <= std_logic_vector(res_int(XLEN-1 downto 0));
   Z <= '1' when to_integer(signed(res_int)) = 0 else '0';
   C <= '1' when (ofchk = "001") or (ofchk = "110") else '0';
   E <= '1' when e_int = true else '0';

end architecture;
